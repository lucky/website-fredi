import axios from 'axios';


class Api {
	constructor(){
		this.url = "http://server-fredi.herokuapp.com"
	//	this.url = "http://localhost:3100"
	}


	login(email, password){
		return axios.post(this.url+"/users/login", {
			email: email,
			password: password
		})
	}

	loginTresorier(email, password){
		return axios.post(this.url+"/tresoriers/login", {
			email: email,
			password: password
		})
	}

	getUser(numLicence){
		return axios.post(this.url+"/tresoriers/getUser", {
			token: window.localStorage.getItem("token"),
			numLicence: numLicence
		})
	}

	validerBordereau(Bordereau){
		return axios.post(this.url+"/tresoriers/validerbordereau", {
			bordereau: Bordereau,
			token: window.localStorage.getItem("token")
		})
	}

// new fonction
	getBordereaux(){
		return axios.post(this.url+"/users/getBordereaux", {
			token: window.localStorage.getItem("token")
		})
	}

	addBordereau(Bordereau){
		return axios.put(this.url+"/users/addBordereau", {
			bordereau: Bordereau,
			token: window.localStorage.getItem("token")

	})
}


	deleteBordereau(bordereau){
		return axios.post(this.url+"/users/deleteBordereau", {
			bordereau : bordereau,
			token: window.localStorage.getItem("token")

		})
	}

	exportBordereau(id){
		return axios({
		  url: this.url+"/users/exportBordereau/"+id+"/"+window.localStorage.getItem("token"),
		  method: 'GET',
		  responseType: 'blob', // important
		})

	}

	register( email, password, numLicence, numMobile, nom, prenom, dateNaissance){
		return axios.put(this.url+"/users/register",{
			email: email,
			password: password,
			numLicence: numLicence,
			numMobile: numMobile,
			nom:nom,
			prenom:prenom,
			dateNaissance:dateNaissance
		})
	}

	supprimerUser(){
		return axios.post(this.url+"/users/supprimerUser", {
			token: window.localStorage.getItem("token")
		})
	}



	checkUser(token){
		return axios.post(this.url+"/users/checkUser", {
			token: token
		})
	}

}


export default Api
