import React, { Component } from 'react';
import { Marker } from "react-google-maps"

const Marker = props => (
	<Marker
	  position={{ lat: props.galerie.lat, lng: props.galerie.long }}
	/>
);

export default Marker