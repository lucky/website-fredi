import React, { Component } from 'react';
import Head from 'next/head';

import Api from '../service/Api'
import { DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown,
NavItem, NavLink, Nav, Collapse, Navbar, NavbarBrand,
NavbarToggler, Button, Form, FormGroup, Label, Input, FormText, Alert } from 'reactstrap';


class Inscription extends Component{


  constructor(props){
    super(props);
    this.api = new Api();
    this.state = {
      isOpen:false,
      email:"",
      password:"",
      numLicence:0,
      numMobile:0,
      nom:"",
      prenom:"",
      dateNaissance:"",
      showBox:{
        show: false,
        success: false,
        message: ""
      }
    }
    this.toggle = this.toggle.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this)
    this.submitinscription = this.submitinscription.bind(this)
  }

  toggle(){
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  handleInputChange(event){
		const name = event.target.name
		const value = event.target.value;
		this.setState({[name]: value})
	}

  submitinscription(){
    this.api.register(this.state.email, this.state.password, this.state.numLicence, this.state.numMobile, this.state.nom, this.state.prenom, this.state.dateNaissance).then(res=>{
      let box = {
        show: true,
        success: res.data.success,
        message: res.data.message
      }
      console.log(res)
      this.setState({showBox: box})
      if(res.data.success){
        setTimeout(()=>{
          window.location.href="/"
        }, 2000);

      }
    })
  }

  render(){

    return (
      <div>
        <Head>
          <link rel="stylesheet" href="/static/styles.css"/>
          <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossOrigin="anonymous"/>
          <title> Inscription </title>
        </Head>

        <Navbar color="dark" light expand="md">
        <NavbarBrand href="/"> M2L - CROSL </NavbarBrand>
        <NavbarToggler onClick={this.toggle} />
            <Collapse isOpen={this.state.isOpen} navbar>
              <Nav className="ml-auto" navbar>
                <NavItem>
                  <NavLink href="./">Se connecter</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="./connexionTresorier">Espace tresorier</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="https://framagit.org/lucky/website-fredi" target="_blank">Framagit</NavLink>
                </NavItem>
                <UncontrolledDropdown nav inNavbar>
                  <DropdownToggle nav caret>
                    Options
                  </DropdownToggle>
                  <DropdownMenu right>
                    <DropdownItem>
                      Option 1
                    </DropdownItem>
                    <DropdownItem>
                      Option 2
                    </DropdownItem>
                    <DropdownItem divider />
                    <DropdownItem>
                      Reset
                    </DropdownItem>
                  </DropdownMenu>
                </UncontrolledDropdown>
              </Nav>
          </Collapse>
        </Navbar>


        <div className="rectangle">
    			   <h2 style={{textAlign:"center"}}>Inscription</h2>

    				<Form>
    				 <FormGroup>
    					 <Label >Email</Label>
    					 <Input type="email" name="email" value={this.state.email} onChange={this.handleInputChange} placeholder="jean@gmail.com" />
    				 </FormGroup>
             <FormGroup>
    					 <Label >Numéro Licence</Label>
    					 <Input type="number" name="numLicence" value={this.state.numLicence} onChange={this.handleInputChange} placeholder="129292929" />
    				 </FormGroup>
    				 <FormGroup>
    					 <Label >Mot de passe</Label>
    					 <Input type="password" name="password" value={this.state.password} onChange={this.handleInputChange} placeholder="*********" />
    				 </FormGroup>
             <FormGroup>
               <Label >N° téléphone</Label>
               <Input type="number" name="numMobile" value={this.state.numMobile} onChange={this.handleInputChange} placeholder="0676541231" />
             </FormGroup>

             <FormGroup>
    					 <Label>Nom</Label>
    					 <Input type="text" name="nom" value={this.state.nom} onChange={this.handleInputChange} placeholder="Dupont" />
    				 </FormGroup>
             <FormGroup>
               <Label>Prénom</Label>
               <Input type="text" name="prenom" value={this.state.prenom} onChange={this.handleInputChange} placeholder="Lucas" />
             </FormGroup>
             <FormGroup>
               <Label>Date de naissance</Label>
               <Input type="text" name="dateNaissance" value={this.state.dateNaissance} onChange={this.handleInputChange} placeholder="jj/mm/aaaa" />
             </FormGroup>

    			 </Form>
    			 <Button type="button" onClick={this.submitinscription} color="secondary">Créer un compte</Button>
           { this.state.showBox.show == true ?
             <Alert color={this.state.showBox.success ? "success" : "danger"}>
                {this.state.showBox.message}
             </Alert> : null
           }

        </div>

      </div>
    )


  }


}


export default Inscription
