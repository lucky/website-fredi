import React, { Component } from 'react';
import Head from 'next/head';
import Api from '../service/Api'

// Les imports bootstrap
import {DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown, Nav, NavItem, NavLink, Navbar, NavbarBrand, NavbarToggler,
	Container, Col, Row, Button, Form, FormGroup, Collapse,
	 Label, Input, FormText, Alert, Table, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Card, CardTitle, CardText} from 'reactstrap';




class Home extends Component {

	constructor(props){
		super(props);

		this.api = new Api();
		this.handleInputChange = this.handleInputChange.bind(this);
		this.ajouterFrais = this.ajouterFrais.bind(this);
		this.supprimerFrais = this.supprimerFrais.bind(this);
    this.openModal = this.openModal.bind(this);
    this.openModalBordereau = this.openModalBordereau.bind(this);
    this.addBordereau = this.addBordereau.bind(this);
		this.getBordereaux = this.getBordereaux.bind(this);

    this.state = {
      user: {},
      modalShow: false,
      modalShowBordereau: false,
			showBox:{
        show: false,
        success: false,
        message: ""
      },
      bordereau: {
        annee: new Date().getFullYear(),
        frais: []
      },
      newFrais: {
        id: 0,
        date: "",
        motif: "",
        trajet: "",
        distance: 0,
        coutTrajet: 0,
        peages: 0,
        repas: 0,
        hebergement:0,
        total: 0
      },
			mesBordereaux: [],
			selectedBordereau: {frais: []}
    }

	}

// fonction d'appel pour aller chercher les bordereaux dans la base de donnée
// new fonction
getBordereaux(){
	this.api.getBordereaux().then(res=>{
		this.setState({mesBordereaux: res.data.bordereaux})
	});
}


  componentDidMount() { // fonction qui est executée autamiquement quand tout le DOM est chargé.
    // vérfions la connexion
    let token = window.localStorage.getItem('token');
    if(!token){
      window.location.href="/";
    } else {
      // si le token existe, envoyons le au serveur pour le vérifier
      this.api.checkUser(token).then(res=>{
        if(res.data.success){
          window.localStorage.setItem("user", JSON.stringify(res.data.user))
          this.setState({user: res.data.user})
					this.getBordereaux();
        }else{
          window.location.href="/";

        }
      })

    }
  }


	handleInputChange(event){
		const name = event.target.name
		const value = event.target.value;
    let newFrais = this.state.newFrais;
    newFrais[name] = event.target.value
		this.setState({"newFrais": newFrais})
	}


  openModal(){
    this.setState(prevState => ({
      modalShow: !prevState.modalShow
    }));
  }

	openModalBordereau(b){
		if(b){
			this.setState({selectedBordereau: b})
		}
    this.setState(prevState => ({
      modalShowBordereau: !prevState.modalShowBordereau
    }));

  }



  ajouterFrais(){
    let frais = this.state.bordereau.frais;
    let newFrais = this.state.newFrais;
    frais.push(newFrais)
    this.state.bordereau.frais = frais;
    this.setState({bordereau: this.state.bordereau})

    this.setState({newFrais: {
            id: 0,
            date: "",
            motif: "",
            trajet: "",
            distance: 0,
            coutTrajet: 0,
            peages: 0,
            repas: 0,
            hebergement:0,
            total: 0
    }})
    this.openModal();
  }

  supprimerFrais(id){
    let frais = this.state.bordereau.frais
    console.log(id)
    frais.splice(id, 1);
    this.state.bordereau.frais = frais;
    this.setState({bordereau: this.state.bordereau})

  }

	addBordereau(){
		if(this.state.bordereau.frais.length!=0){
			this.api.addBordereau(this.state.bordereau).then(res=>{
				let showBox = {
	        show: true,
	        success: res.data.success,
	        message: res.data.message
	      }
				this.setState({showBox: showBox})
				this.setState({bordereau:{
		        date: new Date().getFullYear(),
		        frais: []
				}})
				this.getBordereaux()
			})
		}else{
			let showBox = {
				show: true,
				success: false,
				message: "Ajoutez des frais"
			}
			this.setState({showBox: showBox})
		}


	}

	deleteBordereau(bordereau){
		if (window.confirm("Voulez-vous vraiment supprimer ce bordereau ?")) {
		    this.api.deleteBordereau(bordereau).then(res=>{
					if(res.data.success){
						this.getBordereaux()
					}
				});
		}

	}

	exportBordereau(id){
		this.api.exportBordereau(id).then(res => {
			const url = window.URL.createObjectURL(new Blob([res.data]));
		  const link = document.createElement('a');
		  link.href = url;
		  link.setAttribute('download', 'file.xlsx');
		  document.body.appendChild(link);
		  link.click();
		})

	}

	logout(){
		window.localStorage.removeItem("token")
		window.localStorage.removeItem("user")
		window.location.href = "/"
	}




	render(){
		return (
			<div>
				<Head>
					<title>FREDI</title>
					<link href="/static/styles.css" rel="stylesheet"/>
					<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossOrigin="anonymous"/>

				</Head>

				<Navbar color="dark" light expand="md">
				<NavbarBrand href="/"> M2L - CROSL  - Bienvenue {this.state.user.nom} {this.state.user.prenom} </NavbarBrand>
				<NavbarToggler onClick={this.toggle} />
						<Collapse isOpen={this.state.isOpen} navbar>
							<Nav className="ml-auto" navbar>
								<NavItem>
									<Button color="danger" style={{float:"right"}}onClick={this.logout}>Se déconnecter</Button>
								</NavItem>
								<NavItem>
									<Button color="danger" style={{float:"right", marginLeft: "5px"}}><a href={"./suppression"}>Supprimer données</a></Button>
								</NavItem>
								<NavItem>
									<NavLink href="https://framagit.org/lucky/website-fredi" target="_blank">Framagit</NavLink>
								</NavItem>
								<UncontrolledDropdown nav inNavbar>
									<DropdownToggle nav caret>
										Options
									</DropdownToggle>
									<DropdownMenu right>
										<DropdownItem>
											Option 1
										</DropdownItem>
										<DropdownItem>
											Option 2
										</DropdownItem>
										<DropdownItem divider />
										<DropdownItem>
											Reset
										</DropdownItem>
									</DropdownMenu>
								</UncontrolledDropdown>
							</Nav>
					</Collapse>
			</Navbar>


        <div className="body">
					<Container>
						<Row>
							<Col lg="10">
								<div className="bordereau">
									<Table bordered>
						        <thead style={{"fontWeight":"bold"}}>
										<tr>
						          <th>Date</th>
						          <th>Motif</th>
						          <th>Trajet</th>
						          <th>Distance</th>
						          <th>Coût trajet</th>
						          <th>Péages</th>
						          <th>Repas</th>
						          <th>Hébérg</th>
						          <th>Total</th>
						          <th>Action</th>
										</tr>
						        </thead>
										<tbody>
										{this.state.bordereau.frais.map((frais, index)=>{
											frais.id = index
											console.log(index)
											return(
												<tr key={frais.id}>
								          <td>{frais.date}</td>
								          <td>{frais.motif}</td>
								          <td>{frais.trajet}</td>
								          <td>{frais.distance}</td>
								          <td>{frais.coutTrajet}</td>
								          <td>{frais.peages}</td>
								          <td>{frais.repas}</td>
								          <td>{frais.hebergement}</td>
								          <td>{frais.total}</td>
								          <td><Button onClick={()=>this.supprimerFrais(frais.id)}>Supprimer</Button></td>
								        </tr>
											)
										})}
										</tbody>
						      </Table>

				          <Button outline color="secondary" onClick={this.openModal}>Ajouter frais</Button>
									<div style={{"marginTop": "10px", "textAlign": "center"}}>
										<Button  color="success" onClick={this.addBordereau}>Ajouter le bordereau</Button>
									</div>


								</div>
							</Col>
							<Col lg="2" style={{"paddingTop": "10px"}}>
								<h3>Mes bordereaux</h3>
								<div style={{"height": "460px", "overflow": "auto"}}>
									{this.state.mesBordereaux.map(bordereau=>{
										return (
											// on utilise l'id de la bdd pour différencier les div de bordereau
											<div key={bordereau._id}>
												<Card body style={{"marginTop": "5px"}}>

								           <CardTitle>
													 	Année: {bordereau.annee}
														<Button close aria-label="Supprimer" onClick={()=>this.deleteBordereau(bordereau)} >
								              <span aria-hidden>×</span>
								            </Button>
													 </CardTitle>
								           <Button onClick={()=>this.openModalBordereau(bordereau)}>Afficher</Button>
								         </Card>
											 </div>
										)
									})}
								</div>




							</Col>
						</Row>
					</Container>
					{ this.state.showBox.show == true ?
						<Alert color={this.state.showBox.success ? "success" : "danger"}>
							 {this.state.showBox.message}
						</Alert> : null
					}


        </div>

        <Modal isOpen={this.state.modalShow} toggle={this.openModal} className={this.props.className}>
          <ModalHeader toggle={this.openModal}>Ajouter un frais</ModalHeader>
          <ModalBody>

            <Form>
              <Row form>
                <Col md={6}>
                  <FormGroup>
                    <Label >Date</Label>
                    <Input type="number" name="date" value={this.state.newFrais.date} onChange={this.handleInputChange} placeholder="entrez date" />
                  </FormGroup>
                </Col>
                <Col md={6}>
                  <FormGroup>
                    <Label for="">Trajet</Label>
                    <Input type="text" name="trajet" value={this.state.newFrais.trajet} onChange={this.handleInputChange} placeholder="entrez trajet" />
                  </FormGroup>
                </Col>
              </Row>
              <Row form>
                <Col md={12}>
                  <FormGroup>
                    <Label >Motif</Label>
                    <Input type="text" name="motif" value={this.state.newFrais.motif} onChange={this.handleInputChange} placeholder="motif frais" />
                  </FormGroup>
                </Col>

              </Row>
              <Row form>
                <Col md={6}>
                  <FormGroup>
                    <Label >Distance (km)</Label>
                    <Input type="number" name="distance" value={this.state.newFrais.distance} onChange={this.handleInputChange} placeholder="entrez distance" />
                  </FormGroup>
                </Col>
                <Col md={6}>
                  <FormGroup>
                    <Label>Coût trajet (€)</Label>
                    <Input type="number" name="coutTrajet" value={this.state.newFrais.coutTrajet} onChange={this.handleInputChange} placeholder="entrez cout trajet" />
                  </FormGroup>
                </Col>
              </Row>
              <Row form>
                <Col md={6}>
                  <FormGroup>
                    <Label>Péages  (€)</Label>
                    <Input type="number" name="peages"  value={this.state.newFrais.peages} onChange={this.handleInputChange} placeholder="with a placeholder" />
                  </FormGroup>
                </Col>
                <Col md={6}>
                  <FormGroup>
                    <Label>Repas (€)</Label>
                    <Input type="number" name="repas" value={this.state.newFrais.repas}  onChange={this.handleInputChange} placeholder="entrez repas" />
                  </FormGroup>
                </Col>
              </Row>
              <Row form>
                <Col md={12}>
                  <FormGroup>
                    <Label>hebergement (€)</Label>
                    <Input type="number" name="hebergement" value={this.state.newFrais.hebergement}  onChange={this.handleInputChange} placeholder="entrez hebergement" />
                  </FormGroup>
                </Col>
              </Row>

            </Form>

          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.ajouterFrais}>Ajouter</Button>
            <Button color="secondary" onClick={this.openModal}>Annuler</Button>
          </ModalFooter>
        </Modal>

				<Modal id="bigModal" isOpen={this.state.modalShowBordereau} toggle={()=>this.openModalBordereau(null)} className={this.props.className}>
          <ModalHeader toggle={()=>this.openModalBordereau(null)}>Bordereau {this.state.selectedBordereau.annee}</ModalHeader>
          <ModalBody>
						<Table bordered>
							<thead style={{"fontWeight":"bold"}}>
							<tr>
								<th>Date</th>
								<th>Motif</th>
								<th>Trajet</th>
								<th>Distance</th>
								<th>Coût trajet</th>
								<th>Péages</th>
								<th>Repas</th>
								<th>Hébérgement</th>
								<th>Total</th>
							</tr>
							</thead>
							{this.state.selectedBordereau.frais.map((frais, index)=>{
								frais.id = index
								return(
									<tr key={index}>
										<td>{frais.date}</td>
										<td>{frais.motif}</td>
										<td>{frais.trajet}</td>
										<td>{frais.distance}</td>
										<td>{frais.coutTrajet}</td>
										<td>{frais.peages}</td>
										<td>{frais.repas}</td>
										<td>{frais.hebergement}</td>
										<td>{frais.total}</td>
									</tr>

								)
							})}


						</Table>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={()=>this.exportBordereau(this.state.selectedBordereau._id)}>Exporter</Button>
            <Button color="secondary" onClick={()=>this.openModalBordereau(null)}>Fermer</Button>
          </ModalFooter>
        </Modal>
			</div>

		);
	}
}

export default Home;
