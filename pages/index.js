import React, { Component } from 'react';
import Head from 'next/head';

import Api from '../service/Api'

// Les imports bootstrap
import {
	DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown,
	NavItem, NavLink, Nav, Collapse, Navbar, NavbarBrand,
	NavbarToggler, Button, Form, FormGroup, Label, Input, FormText, Alert } from 'reactstrap';

class Connexion extends Component {

	constructor(props){
		super(props);
		this.api = new Api();
		this.state  = {
			isOpen: false,
			email: "",
			password: "",
			showBox:{
        show: false,
        success: false,
        message: ""
      }
		}
		this.toggle = this.toggle.bind(this);
		this.handleInputChange = this.handleInputChange.bind(this);
		this.submitConnexion = this.submitConnexion.bind(this);

	}

	toggle(){
		this.setState({
			isOpen: !this.state.isOpen
		});
	}

	handleInputChange(event){

		const name = event.target.name
		const value = event.target.value;
		this.setState({[name]: value})
	}


	submitConnexion(){
		this.api.login(this.state.email, this.state.password).then(res=>{
			let box = {
        show: true,
        success: res.data.success,
        message: res.data.message
      }
      this.setState({showBox: box})
			if(res.data.success){
				// maintenant on stocke le token et le user dans localstorage
				window.localStorage.setItem("user", JSON.stringify(res.data.user))
				window.localStorage.setItem("token", res.data.token)
				window.location.href = "/home"
			}

		})
	}


	render(){


		return (
			<div>
				<Head>
					<title>FREDI</title>
					<link href="/static/styles.css" rel="stylesheet"/>
					<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossOrigin="anonymous"/>

				</Head>

					<Navbar color="dark" light expand="md">
					<NavbarBrand href="/"> M2L - CROSL </NavbarBrand>
					<NavbarToggler onClick={this.toggle} />
					    <Collapse isOpen={this.state.isOpen} navbar>
								<Nav className="ml-auto" navbar>
									<NavItem>
										<NavLink href="./inscription">S'inscrire</NavLink>
									</NavItem>
									<NavItem>
										<NavLink href="./connexionTresorier">Espace tresorier</NavLink>
									</NavItem>
									<NavItem>
										<NavLink href="https://framagit.org/lucky/website-fredi" target="_blank">Framagit</NavLink>
									</NavItem>
									<UncontrolledDropdown nav inNavbar>
										<DropdownToggle nav caret>
											Options
										</DropdownToggle>
										<DropdownMenu right>
											<DropdownItem>
												Option 1
											</DropdownItem>
											<DropdownItem>
												Option 2
											</DropdownItem>
											<DropdownItem divider />
											<DropdownItem>
												Reset
											</DropdownItem>
										</DropdownMenu>
									</UncontrolledDropdown>
								</Nav>
						</Collapse>
				</Navbar>


				<div className="rectangle">
					<h2 style={{textAlign:"center"}}>Connexion</h2>

						<Form>
						 <FormGroup>
							 <Label for="exampleEmail">Email</Label>
							 <Input type="email" name="email" value={this.state.email} onChange={this.handleInputChange} placeholder="enter mail" />
						 </FormGroup>
						 <FormGroup>
							 <Label for="examplePassword">Password</Label>
							 <Input type="password" name="password" value={this.state.password} onChange={this.handleInputChange} placeholder="enter password" />
						 </FormGroup>


					 </Form>
					 { this.state.showBox.show == true ?
             <Alert color={this.state.showBox.success ? "success" : "danger"}>
                {this.state.showBox.message}
             </Alert> : null
           }
					 <Button type="button" onClick={this.submitConnexion} color="secondary">Se connecter</Button>

				</div>


			</div>

		);
	}
}

export default Connexion;
