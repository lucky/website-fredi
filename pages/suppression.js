import React, { Component, } from 'react';
import Head from 'next/head';
import { DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown,
NavItem, NavLink, Nav, Collapse, Navbar, NavbarBrand,
NavbarToggler, CardTitle, Card, Button, Row, Col, Alert, Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';

import Api from '../service/Api'


class Suppression extends Component {

	constructor(props){
		super(props);

	  this.api = new Api();

		this.supprimerUser = this.supprimerUser.bind(this);
	  this.exportUserData = this.exportUserData.bind(this);
		this.toggle = this.toggle.bind(this);



		this.state = {
			modal: false,
			user:{},
			showBox:{
				show: false,
				success: false,
				message: ""
			}
		}

	}


 toggle() {
	 this.setState(prevState => ({
		 modal: !prevState.modal
	 }));
 }

	componentDidMount() { // fonction qui est executée autamiquement quand tout le DOM est chargé.
		// vérfions la connexion
		let token = window.localStorage.getItem('token');
		if(!token){
			window.location.href="/";
		} else {
			// si le token existe, envoyons le au serveur pour le vérifier
			this.api.checkUser(token).then(res=>{
				if(res.data.success){
					window.localStorage.setItem("user", JSON.stringify(res.data.user))
					this.setState({user: res.data.user})
				}else{
					window.location.href="/";

				}
			})

		}
	}



supprimerUser(){
	this.toggle();
  this.api.supprimerUser().then(res=>{
		let box = {
			show: true,
			success: res.data.success,
			message: res.data.message
		}
		this.setState({showBox: box})
		if(res.data.success){
			setTimeout(this.logout, 3000)
		}
	})
}

 convertToCSV(objArray) {

    var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    var str = '';


		var keys = Object.keys(objArray[0]);
		let entete = ''
		for(let key of keys){
			entete += key+";"
		}
		console.log(entete)
    for (var i = 0; i < array.length; i++) {

        var line = '';
        for (var index in array[i]) {

            if (line != '') line += ';'

            line += array[i][index];
						console.log(line)
        }

        str += entete + '\r\n' +line
    }

    return str;
}


  exportUserData(){
	  let user = JSON.parse(window.localStorage.user)
		console.log(user)
		let csvfile = this.convertToCSV([user]);

		let $a = document.createElement('a');
		$a.href = 'data:text/plain;charset=utf-8,' + encodeURIComponent(csvfile);
		$a.download = user.nom+'-'+user.email+'.csv';
		$a.style.display = 'none';
		document.body.appendChild($a);
		$a.click();
		$a.remove();


  }

	logout(){
		window.localStorage.removeItem("token")
		window.localStorage.removeItem("user")
		window.location.href = "/"
	}


render(){
 return(
   <div>

	     <Head>
	       <link rel="stylesheet" href="/static/styles.css"/>
	       <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossOrigin="anonymous"/>
	       <title> Suppression </title>
	     </Head>


				 	<Navbar color="dark" light expand="md">
			 		<NavbarBrand href="/"> M2L - CROSL - Paramètres utilisateur de {this.state.user.nom} {this.state.user.prenom}</NavbarBrand>
			 		<NavbarToggler onClick={this.toggle} />
			 			<Collapse isOpen={this.state.isOpen} navbar>
			 				<Nav className="ml-auto" navbar>
			 					<NavItem>
			 						<Button color="danger" style={{float:"right"}}onClick={this.logout}>Se déconnecter</Button>
			 					</NavItem>
			 					<NavItem>
			 						<NavLink href="./connexionTresorier">Espace tresorier</NavLink>
			 					</NavItem>
			 					<NavItem>
			 						<NavLink href="https://framagit.org/lucky/website-fredi" target="_blank">Framagit</NavLink>
			 					</NavItem>
			 					<UncontrolledDropdown nav inNavbar>
			 						<DropdownToggle nav caret>
			 							Options
			 						</DropdownToggle>
			 						<DropdownMenu right>
			 							<DropdownItem>
			 								Option 1
			 							</DropdownItem>
			 							<DropdownItem>
			 								Option 2
			 							</DropdownItem>
			 							<DropdownItem divider />
			 							<DropdownItem>
			 								Reset
			 							</DropdownItem>
			 						</DropdownMenu>
			 					</UncontrolledDropdown>
			 				</Nav>
			 		</Collapse>
			 	</Navbar>

			 <Card >
					<CardTitle>Actions données utilisateur</CardTitle>
			    <div style={{textAlign: "center"}}>
				      <Button color="danger" onClick={this.toggle}>Supprimer</Button>
				      <Button color="secondary" onClick={this.exportUserData}>Exporter</Button>
			    </div>

					<Row >
							<Col md={5}>
							{ this.state.showBox.show == true ?
								<Alert color={this.state.showBox.success ? "success" : "danger"}>
									 {this.state.showBox.message}
								</Alert> : null
							}
							</Col>
					</Row>

				</Card>

				<Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle}>Suppression données utilisateur</ModalHeader>
          <ModalBody>
					Voulez-vous vraiment supprimer vos données?
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.supprimerUser}>Oui</Button>{' '}
            <Button color="secondary" onClick={this.toggle}>Annuler</Button>
          </ModalFooter>
        </Modal>

	  </div>

);
}



}

export default Suppression
