import React, { Component } from 'react';
import Head from 'next/head';
import Api from '../service/Api'

// Les imports bootstrap
import {DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown,
NavItem, NavLink, Nav, Collapse, Navbar, NavbarBrand,
NavbarToggler, Container, Col, Row, Button, Form, FormGroup, Label, Input, FormText, Alert, Table, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Badge, Card, CardBody, CardSubtitle, CardTitle, CardText} from 'reactstrap';
import { ListGroup, ListGroupItem } from 'reactstrap';



class Home extends Component {

	constructor(props){
		super(props);



		this.api = new Api();
    this.state = {
      tresorier: {},
      numLicence: 0,
			modalShowBordereau: false,
			selectedBordereau: {frais:[]},
			showBox:{
        show: false,
        success: false,
        message: ""
      },
			searchUser: null,
    }

    this.getUser = this.getUser.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
		this.getUser = this.getUser.bind(this);
		this.openModalBordereau = this.openModalBordereau.bind(this);

	}


  handleInputChange(event){
		const name = event.target.name
		const value = event.target.value;
		this.setState({[name]: value})
	}


  getUser(){
    this.api.getUser(this.state.numLicence).then(res=>{
      if(res.data.success){
				let user = res.data.user;
				user["bordereaux"] = res.data.bordereaux;
				this.setState({searchUser: res.data.user})
				this.setState({tresorier: res.data.user})
			}else{
				alert(res.data.message)
			}
    })
  }

	openModalBordereau(bordereau){
		if(bordereau!=null){
			this.setState(prevState=>({
				modalShowBordereau: !prevState.modalShowBordereau,
				selectedBordereau: bordereau
			}))
		}else{
			this.setState(prevState=>({
				modalShowBordereau: !prevState.modalShowBordereau
			}))
		}


	}

	validateBordereau(bordereau){
		this.openModalBordereau(null);
		if (window.confirm("Voulez-vous vraiment valider ce bordereau ?")) {
			this.api.validerBordereau(bordereau).then(res => {
			console.log(res);
			this.setState({selectedBordereau:bordereau});
			})
		}
	}


	logout(){
		window.localStorage.removeItem("token")
		window.localStorage.removeItem("tresorier")
		window.location.href = "/"
	}

	render(){
		return (
			<div>
				<Head>
					<title>FREDI</title>
					<link href="/static/styles.css" rel="stylesheet"/>
					<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossOrigin="anonymous"/>
				</Head>


				<Navbar color="dark" light expand="md">
				<NavbarBrand href="/"> M2L - CROSL - {this.state.tresorier.nom} {this.state.tresorier.prenom}</NavbarBrand>
				<NavbarToggler onClick={this.toggle} />
						<Collapse isOpen={this.state.isOpen} navbar>
							<Nav className="ml-auto" navbar>
								<NavItem>
									<Button color="danger" style={{float:"right"}}onClick={this.logout}>Se déconnecter</Button>
								</NavItem>
								<NavItem>
									<NavLink href="https://framagit.org/lucky/website-fredi" target="_blank">Framagit</NavLink>
								</NavItem>
								<UncontrolledDropdown nav inNavbar>
									<DropdownToggle nav caret>
										Options
									</DropdownToggle>
									<DropdownMenu right>
										<DropdownItem>
											Option 1
										</DropdownItem>
										<DropdownItem>
											Option 2
										</DropdownItem>
										<DropdownItem divider />
										<DropdownItem>
											Reset
										</DropdownItem>
									</DropdownMenu>
								</UncontrolledDropdown>
							</Nav>
					</Collapse>
				</Navbar>


        <div className="body">

					<div className="marginTop">
	          <Row>
	           	<Col sm="12" md={{ size: 6, offset: 3 }}>
							<Card>
	            <Form >
	              <FormGroup style={{textAlign:"center"}}>
	                <Label for="Adhérent">Recherche Adhérent</Label>
	                <Input type="number" name="numLicence" value={this.state.numLicence} onChange={this.handleInputChange} placeholder="Entrez Numéro de licence" style={{width:"500px"}} />
	              </FormGroup>
	              </Form>
	              <Button onClick={this.getUser} style={{width:"150px"}}> Rechercher </Button>
								</Card>
	            </Col>
	          </Row>
					</div>

          <div style={{marginTop: "50px"}}>
					{this.state.searchUser ?
						<Row>
								<Col sm="12" md={{ size: 6, offset: 3 }}>
									<Card>
										<CardBody>
											<CardTitle>{this.state.searchUser.prenom} {this.state.searchUser.nom}</CardTitle>
											<CardSubtitle>Numéro Licence : {this.state.searchUser.numLicence}</CardSubtitle>
											<CardText>email : {this.state.searchUser.email}</CardText>
											<ListGroup>

												{this.state.searchUser.bordereaux.map(bordereau=>{
													return (
														<ListGroupItem key={bordereau._id} tag="button" onClick={()=>this.openModalBordereau(bordereau)} action> Bordereau n°: {bordereau._id} - {bordereau.annee}
															 { bordereau.validated == true ?
																	<Badge  style={{float:"right"}}color="success">
																		 validé ✓
																	</Badge> : null
																}
														</ListGroupItem>

													)
												})}
							        </ListGroup>
										</CardBody>
									</Card>
								</Col>
						</Row>
						: null
					}

            </div>

          </div>

					<Modal  id="bigModal" isOpen={this.state.modalShowBordereau} toggle={()=>this.openModalBordereau(null)} className={this.props.className}>
	          <ModalHeader toggle={()=>this.openModalBordereau(null)}>Bordereau {this.state.selectedBordereau.annee}</ModalHeader>
	          <ModalBody>

						<Table bordered>
							<thead style={{"fontWeight":"bold"}}>
							<tr>
								<th>Date</th>
								<th>Motif</th>
								<th>Trajet</th>
								<th>Distance</th>
								<th>Coût trajet</th>
								<th>Péages</th>
								<th>Repas</th>
								<th>Hébérgement</th>
								<th>Total</th>
								</tr>
							</thead>
							<tbody>
							{this.state.selectedBordereau.frais.map((frais, index)=>{
								frais.id = index
								return(
									<tr key={index}>
										<td>{frais.date}</td>
										<td>{frais.motif}</td>
										<td>{frais.trajet}</td>
										<td>{frais.distance}</td>
										<td>{frais.coutTrajet}</td>
										<td>{frais.peages}</td>
										<td>{frais.repas}</td>
										<td>{frais.hebergement}</td>
										<td>{frais.total}</td>
									</tr>

								)
							})}
							</tbody>
							</Table>


	          </ModalBody>
	          <ModalFooter>

							<Button color="danger" >Corriger</Button>

							{this.state.selectedBordereau.validated == false ?
								  <Button onClick={()=>this.validateBordereau(this.state.selectedBordereau)} color="success" >Valider</Button>
									: null
							}
	            <Button color="secondary" onClick={()=>this.openModalBordereau(null)}>Fermer</Button>
	          </ModalFooter>
	        </Modal>
			</div>

		);
	}
}

export default Home;
